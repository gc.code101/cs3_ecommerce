import { useState, useEffect} from 'react';
import Hero from '../components/Banner';
import {Container} from 'react-bootstrap';
import ProductCard from '../components/ProductCard';


const infoDetails = {
	title: "Welcome to the Shop Page!",
	description: "You can enjoy shopping in this page",
	callToAction: "Shop Now!"
}

export default function Products() {

	const [ products, setProducts] = useState([]);

	useEffect(() => {
		fetch('https://morning-bastion-91045.herokuapp.com/products/active')
		.then(result => result.json())
		.then(listOfProducts => {	
			setProducts(listOfProducts.map(product => {
				return (
					<ProductCard key={product._id} productDetails={product} />
				)
			}));

		})

	});

	return (
		<Container className="mt-5">
			<Hero intro={infoDetails} />
			<div className="d-flex flex-wrap">{products}</div>

		</Container>
		
	)
};