import Hero from '../components/Banner';
import {Container, Carousel} from 'react-bootstrap';

let introDetails = {
	title: "DREWit CLOTHING!",
	description: "This Ecommerce app will help you shop from the comfort of your home.",
	callToAction: `Show it.
		Wear it.
		Live it.
		Love it.
		#JustDREWit`
}

export default function Home() {
	return (
		<Container className="mt-5">
			<Hero intro={introDetails}/>
			<Carousel>
				<Carousel.Item>
					<img className="d-block w-100" src="drewit.jpg" />
				</Carousel.Item>
				<Carousel.Item>
					<img className="d-block w-100" src="babaero.jpg" />
				</Carousel.Item>
				<Carousel.Item>
					<img className="d-block w-100" src="fast.jpg" />
				</Carousel.Item>
				<Carousel.Item>
					<img className="d-block w-100" src="pogi.jpg" />
				</Carousel.Item>
				<Carousel.Item>
					<img className="d-block w-100" src="sabong.jpg" />
				</Carousel.Item>
				<Carousel.Item>
					<img className="d-block w-100" src="sexy.jpg" />
				</Carousel.Item>
				<Carousel.Item>
					<img className="d-block w-100" src="sizes.jpg" />
				</Carousel.Item>
				<Carousel.Item>
					<img className="d-block w-100" src="speed.jpg" />
				</Carousel.Item>
			</Carousel>
		</Container>
	)
}