import {useState, useEffect, useContext} from 'react';
import Hero from '../components/Banner';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import { Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';



const infoDetails = {
	title: "Review Product",
	description: "this helps the users to get a clear idea of the product before purchasing it.",
	callToAction: "Purchase this product NOW!"
}


export default function ProductReview() {
	
	const { user } = useContext(UserContext);
	const { targetId }: { targetId: string } = useParams();

	const [prodId, setProduct] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	

	useEffect(() => {
		
		fetch(`https://morning-bastion-91045.herokuapp.com/products/${targetId}`)
		.then(outcome => outcome.json())
		.then(lists => {
			console.log(lists);
			setProduct(lists._id);
			setName(lists.name);
			setDescription(lists.description);
			setPrice(lists.price);
			
		}).catch(err => console.log(err))// useEffect
		
	}, [targetId]) //export
	
	function addToCart() {
		Swal.fire({
				title: `Hi! This is under development`,
				icon: 'info',
				text: 'Available Soon!'
			})
	}

	return (
		<Container className="mt-5">
			<Hero intro={infoDetails} />
			<div>
				<Row>	
					<Col xs={12}>
						<img src="../tshirt.jpg" className="reviewSize mx-auto d-block" alt="not found"/>
					</Col>

					<Col xs={12}>
						<Card>
							<Card.Body>
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Product Id: </Card.Subtitle>
								<Card.Text>{prodId}</Card.Text>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>PHP {price}</Card.Text>

								{(user.id) ?
									<>
										<Button className="btn btn-primary mr-2" type="submit" id="submitBtn" onClick={() => addToCart()}>Add to Cart</Button>
										<Link className="btn btn-primary" to="/shop">Back to Shop</Link>
									</>
									:
									<>
										<Link className="btn btn-primary mr-2" to="/login">Log in to Buy</Link>
										<Link className="btn btn-primary" to="/shop">Back to  Shop</Link>
									</>
								}	
							</Card.Body>
							<img src="../sizechart.jpg" className="chartSize mt-5 mx-auto d-block" />
						</Card>
						
					</Col>
				</Row>
			</div>		
		</Container>
	)
}