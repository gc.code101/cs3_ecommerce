import UserContext from '../UserContext';
import { useState, useEffect, useContext } from 'react';
import Hero from '../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Redirect, useHistory} from 'react-router-dom';


const infoDetails = {
	title: "Login Here!",
	description: "Please enter your registered credentials.",
	callToAction: "If no account, go to Register Page and create your account NOW!"
}


export default function Login() {

	const {user, setUser} = useContext(UserContext)

	const history = useHistory();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	//effects
	const [isActive, setIsActive] = useState(false);
	const [isComplete, setIsComplete] = useState(false);

	const authenticate = (event) => {
		event.preventDefault();

		fetch('https://morning-bastion-91045.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (typeof data.accessToken !== "undefined") {
				localStorage.setItem('accessToken', data.accessToken);

				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: 'Sucessful Logged In',
					icon: 'success',
					text: 'You can now shop online!'
				});
			} else {
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Check your login details and try again'
				});
				history.push('/login');
			}
		})
	}

	const retrieveUserDetails = (token) => {
		fetch('https://morning-bastion-91045.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(outcome => outcome.json())
		.then(result => {
			setUser({
				id: result._id,
				firstName: result.firstName,
				isAdmin: result.isAdmin
			});
		})
	}

	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(true);
			setIsComplete(true);
		} else {
			setIsActive(false);
			setIsComplete(false);
		}
	}, [email, password]);

	return (

		(user.id) ?
			<Redirect to="/shop" />
			:
			<Container className="mt-5" xs={12}>
				<Hero intro={infoDetails} />

				<Form onSubmit={(event) => authenticate(event)} className="loginDesign p-5">
					{isComplete ?
						<h3 className="text-success mb-4">Click the "Login" Button</h3>
						:
						<h3 className="text-primary mb-4">Log In</h3>
					}
					<Form.Group controlId="email">
						<Form.Label>Email Address:</Form.Label>
						<Form.Control type="email" placeholder="Enter your email address" value={email} onChange={e => setEmail(e.target.value)} required />
					</Form.Group>

					<Form.Group controlId="password">
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter your registered password" value={password} onChange={e => setPassword(e.target.value)} required />
					</Form.Group>

					{isActive ?
						<Button type="submit" variant="success" id="submitBtn" className="btn btn-block mt-4">Login</Button>
						:
						<Button type="submit" variant="primary" id="submitBtn" className="btn btn-block mt-4" disabled>Login</Button>

					}
					

				</Form>
			</Container>
	)
}