import Hero from '../components/Banner';
import {Container} from 'react-bootstrap';

const infoDetails = {
	title: "Page Not Found!",
	description: "You are trying to access a Non existing Page",
	callToAction: "404 Error!"
}

export default function Error() {
	return(
		<Container className="mt-5">
			<Hero intro={infoDetails} />
		</Container>
	)
}