import { Link } from 'react-router-dom';
import {Card, Row, Col, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ProductCard({productDetails}) {
	const {_id, name, description, price} = productDetails;

	function addToCart() {
		Swal.fire({
				title: `Hi! This is under development`,
				icon: 'info',
				text: 'Available Soon!'
			})
	}

	return (
		<Row xs={12} md={4}>
			<Col className="p-5">
				<Card className="cardSize">
					<Card.Img variant="top" src="tshirt.jpg" className="imageSize mx-auto d-block mt-4" fluid="true"/>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PHP {price}</Card.Text>
						<Col>
						{<Link className="btn btn-primary mr-2" to={`/products/${_id}`}>
							View Specs	
						</Link>}
						
						<Button className="btn btn-primary" type="submit" id="submitBtn" onClick={() => addToCart()}>Add to Cart</Button>	
						</Col>
						
					</Card.Body>
				</Card>	
			</Col>
		</Row>
	)
}