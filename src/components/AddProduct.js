import {useState, useEffect, useContext} from 'react';
import {Container, Form, Row, Col, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Redirect, useHistory} from 'react-router-dom';
import UserContext from '../UserContext';


export default function AddProduct() {
	const history = useHistory();
	const { user } = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [isComplete, setIsComplete] = useState(false);
	const [isBtnActive, setIsBtnActive] = useState(false);

	console.log(localStorage.accessToken);

	function addNewProduct(event) {
		event.preventDefault();

		fetch('https://morning-bastion-91045.herokuapp.com/users/details', {
		  headers: {
		    Authorization: `Bearer ${ localStorage.getItem('accessToken') }` //concatenated
		  } //upon sending this request a promise will be created
		})
		.then(resultOfPromise => resultOfPromise.json())
		.then(result => {
			console.log(result);
			if (result.isAdmin === true) {
				fetch('https://morning-bastion-91045.herokuapp.com/products/create', {
					method: "POST",
					headers: {
						Authorization: `Bearer ${ localStorage.getItem('accessToken') }`,
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						name: name,
						description: description,
						price: price
					})
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire ({
						title: `Product is added successfully`,
						icon: 'success',
						text: 'New Product Has Been Created'
					})
					history.push('/admin');
					setName('');
					setDescription('');
					setPrice('')
				})
			} else {
				Swal.fire ({
					title: `Unable to add New Product`,
					icon: 'error',
					text: 'Make sure you are an Admin'
				})
				history.push('/admin');
				setName('');
					setDescription('');
					setPrice('')
			}
		})

	}


	useEffect(() => {
		if (name !== '' && description !== '' && price !== '') {
	    	setIsComplete(true);
			setIsBtnActive(true);
	
		} else {
			setIsComplete(false);
			setIsBtnActive(false);
		}
	},[name, description, price])

	return (

		(user.isAdmin === true) ?
			<Container>
				<Row className="mb-5" xs={12}>
					<Col>
						<Form onSubmit={(event) => addNewProduct(event)} className="createProduct p-5">
							{isComplete ?
								<h3>Click the "Add Product" Button</h3>
								:
								<h3>Create New Product</h3>
							}
							
							<Form.Group controlId="name">
								<Form.Label>Product Name: </Form.Label>
								<Form.Control type="text" placeholder="Enter the Product Name Here" value={name} onChange={event => setName(event.target.value)} required />
							</Form.Group>

							<Form.Group controlId="description">
								<Form.Label>Product Description:</Form.Label>
								<Form.Control type="text" placeholder="Enter the Product Description Here" value={description} onChange={event => setDescription(event.target.value)} required />
							</Form.Group>

							<Form.Group controlId="price">
								<Form.Label>Product Price:</Form.Label>
								<Form.Control type="number" placeholder="Enter the Price Here" value={price} onChange={event => setPrice(event.target.value)} required />
							</Form.Group>

							{isBtnActive ?
								<Button type="submit" id="submitBtn" variant="success" className="btn btn-block mt-5" >Add product</Button>
								:
								<Button type="submit" id="submitBtn" variant="primary" className="btn btn-block mt-5" disabled>Add product</Button>
							}
						</Form>
					</Col>	
				</Row>
			</Container>
			:

			<>
			</>
	)

}
