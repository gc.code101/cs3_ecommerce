import { Row, Col } from 'react-bootstrap';

export default function Banner({intro}) {
	const { title, description, callToAction } = intro;
	return (
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{description}</p>
				<h5>{callToAction}</h5>
			</Col>
		</Row>
	)
};